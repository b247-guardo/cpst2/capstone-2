const User = require("../models/User");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Ordered products by user
module.exports.createOrder = (data) => {

	if (!data.isAdmin) {
		let newOrder = new Order({
			userId: data.body.userId,
			products: [{
				productId: data.body.productId,
				quantity: data.body.quantity
			}],
			totalAmount: data.body.totalAmount,
		});

		return newOrder.save().then((order, error) => {
			if (error) {
				return false;
			} else {
				return order;
			};
		});
	} else {
		return Promise.resolve(false);
	};
};

// Retrieve all user's orders
module.exports.getAllOrders = (data) => {

	if (data.isAdmin) {
		return Order.find({}).then(result => {
			return result;
		});
	} else {
		return Promise.resolve(false);
	};
};

// Retrieve authenticated user's orders
module.exports.getMyOrders = (data) => {

	if (!data.isAdmin) {

		return Order.find({ userId : data.userId }).then(order => {
			return order;
		});
	} else {
		return Promise.resolve(false);
	};
};

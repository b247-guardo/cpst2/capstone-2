const Product = require("../models/Product");
const auth = require("../auth");

// Check duplicate products
module.exports.checkDuplicate = (reqBody) => {

	return Product.find({name: reqBody.name}).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		};
 	});
};

// Creating a sample product (Admin)
module.exports.createProduct = (data) => {
 
 	if (data.isAdmin) {

	  	const newProduct = new Product({
		    name: data.product.name,
		    description: data.product.description,
		    price: data.product.price,
	  	});

	  	return newProduct.save().then((result, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
	  	});
  	} else {
  		return Promise.resolve(false);
  	};
};

			
// Retrieving all products
module.exports.getAllProduct = () => {

	return Product.find({}).then(result => {
		return result;
	});
};

// Retrieving all active products
module.exports.getAllActive = () => {

	return Product.find({isActive : true}).then(result => {
		return result;
	});
};

// Retrieving single product
module.exports.getSingleProduct = (reqParams) => {

	return Product.findById(reqParams.id).then((product, error) => {
		if (error) {
			return false;
		} else {
			return product;
		};
	});
};

// Updating a product (Admin)
module.exports.updateProduct = (data) => {

	if (data.isAdmin) {

		let updatedProduct = {
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		};

		return Product.findByIdAndUpdate(data.id, updatedProduct).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	} else {
		return Promise.resolve(false);
	};
};

			
// Archiving a product (Admin)
module.exports.archiveProduct = (data) => {

	if (data.isAdmin) {

		const isActive = {isActive: data.product.isActive};

		return Product.findByIdAndUpdate(data.id, isActive).then((archive, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	} else {
		return Promise.resolve(false);
	};
};
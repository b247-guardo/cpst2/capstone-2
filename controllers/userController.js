const User = require("../models/User");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Checking email
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		};
 	});
};

// Retrieve all users
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result;
	});
};

// Registers a user
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		username: reqBody.username,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	});

	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};

// Authenticates a user
module.exports.authenticateUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if (result == null) {
			return false;
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {
				return {access : auth.createAccessToken(result)};
			} else {
				return false;
			};
		};
	});
};

// Retrieve user details
module.exports.userDetails = (data) => {

	return User.findById(data.id).then(result => {
		if (result === null) {
			return false;
		} else {
			result.password = "";
			return result;
		}
	});
};

// Set "user" as an "admin"
module.exports.setAsAdmin = (data) => {

	if (data.isAdmin) {

		const updatedUser = {isAdmin : data.user.isAdmin};

		return User.findByIdAndUpdate(data.id, updatedUser).then(result => {
			return result;
		});
	} else {
		return Promise.resolve(false);
	};
};
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

const app = express();
const port = 4002;

mongoose.connect("mongodb+srv://guardoej18:darabz312@zuitt-bootcamp.d2vle2v.mongodb.net/s42-s46?retryWrites=true&w=majority",
				{
					useNewUrlParser : true,
					useUnifiedTopology : true
				});

mongoose.connection.once("open", () => console.log(`Now connected to the cloud database: MongoDB Atlas`));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended : true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

app.listen(port, () => console.log(`Server API is now running on port: ${port}`));
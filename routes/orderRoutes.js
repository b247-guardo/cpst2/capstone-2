const express = require("express");
const router = express.Router();
const auth = require("../auth");

const orderController = require("../controllers/orderController");

// Route for user creating an order
router.post("/checkout", auth.verify, (req, res) => {

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		body : req.body,
	};
	
	orderController.createOrder(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all orders
router.get("/orders", auth.verify, (req, res) => {

	const data = {isAdmin : auth.decode(req.headers.authorization).isAdmin}

	orderController.getAllOrders(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving authenticated user's orders
router.get("/myOrders", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	};

	orderController.getMyOrders(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
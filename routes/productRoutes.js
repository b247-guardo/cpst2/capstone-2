const express = require("express");
const router = express.Router();
const auth = require("../auth");

const productController = require("../controllers/productController");

// Route for checking duplicate products
router.post("/checkDuplicate", (req, res) => {

	productController.checkDuplicate(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for creating a sample product (Admin)
router.post("/addProducts", (req, res) => {

	const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	};
	
	productController.createProduct(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all products
router.get("/all", (req, res) => {

	productController.getAllProduct().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all active products
router.get("/", (req, res) => {

	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving single product
router.get("/:id", (req, res) => {

	productController.getSingleProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating products (Admin)
router.put("/:id", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		product: req.body,
		id : req.params.id
	};

	productController.updateProduct(data).then(resultFromController => {
		if (resultFromController === true) {
			res.status(200).json(resultFromController);
		} else {
			res.status(500).json(resultFromController);
		}
	});
});

// Route for archiving products (Admin)
router.put("/:id/archive", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		product: req.body,
		id: req.params.id
	};

	productController.archiveProduct(data).then(resultFromController => {
		if (resultFromController === true) {
			res.status(200).json(resultFromController);
		} else {
			res.status(500).json(resultFromController);
		}
	});
});

module.exports = router;
const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userController");

// Route for checking email
router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})


// Route for retrieving all users
router.get("/get", (req, res) => {

	userController.getAllUsers().then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details
router.get("/userDetails", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);
	
	userController.userDetails(data).then(resultFromController => res.send(resultFromController));
});

// Route for setting "User" as an "Admin"
router.put("/:id/setAsAdmin", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		user: req.body,
		id: req.params.id
	};

	userController.setAsAdmin(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;